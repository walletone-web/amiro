# README #
This payment driver is designed to receive payment orders for CMS Amoro

=== Wallet One Checkout ===
Contributors: h-elena
Version: 2.1
Requires at least: 7.0.0.14
Tested up to: 7.0.0.14
Stable tag: 7.0.0.14
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for CMS Amiro.

== Description ==
Если вы имеете интернет-магазин на CMS Amiro, то вам необходим плагин для осуществления платежей за сделанные заказы. В этом вам поможет драйвер платежной системы Wallet One. С помощью нашей системы вы сможете значительно расширить способы приемы оплаты, а это приведет к увеличению количества клиентов вашего магазина. 

== Installation ==
1. Регистрируемся на сайте https://www.walletone.com/merchant/client/#/signup
2. Загружаем драйвер в админке.
3. Инструкцию читаем на сайте https://www.walletone.com/ru/merchant/modules/amiro/.

== Screenshots ==

== Changelog ==
= 1.1 =
* Fix - фиксация бага, связанного с ответом для платежной системы

= 1.2 =
* Fix - фиксация бага, связанного с проверкой подписи

= 2.0 =
* Fix - фиксация бага, связанного с изменением ответа от платежной системы и добавление универсальных классов

= 2.1 = 
* 54 fz support, change expired date for invoice

== Frequently Asked Questions ==
No recent asked questions
